<h1>Travis CI</h1> 

<h1>Oscar Barajas Tavares</h1> 

<h1>Table of Contents</h1> 

- [1. Getting Started](#1-getting-started)
  - [Todo lo que aprenderás sobre TravisCI](#todo-lo-que-aprenderás-sobre-travisci)
  - [Continuous Integration (CI) y Continuous Delivery (CD)](#continuous-integration-ci-y-continuous-delivery-cd)
  - [Crear cuenta en Travis-ci.org](#crear-cuenta-en-travis-ciorg)
- [2. Configuración](#2-configuración)
  - [Archivo de configuración travis.yml](#archivo-de-configuración-travisyml)
  - [Archivo de configuración travis.yml jobs y deploy](#archivo-de-configuración-travisyml-jobs-y-deploy)
- [3. Deploy Github Pages](#3-deploy-github-pages)
  - [Configuración de travis.yml](#configuración-de-travisyml)
  - [Test & Deploy de Platzi Store](#test--deploy-de-platzi-store)
- [4. Integración y Notificaciones](#4-integración-y-notificaciones)
  - [Integración con notificaciones de correo electrónico](#integración-con-notificaciones-de-correo-electrónico)
  - [Integración con notificaciones de Slack](#integración-con-notificaciones-de-slack)
- [5. Deploy Heroku](#5-deploy-heroku)
  - [Configuración de Integración](#configuración-de-integración)
  - [Configuración de travis.yml con Heroku](#configuración-de-travisyml-con-heroku)
  - [Deploy de Platzi Store Backend](#deploy-de-platzi-store-backend)
- [6. Seguridad](#6-seguridad)
  - [Instalar Travis CLI (Command Line Client)](#instalar-travis-cli-command-line-client)
  - [Buenas practicas de seguridad](#buenas-practicas-de-seguridad)
- [7. Travis CI Enterprise](#7-travis-ci-enterprise)
  - [Travis CI Enterprise y cierre del proyecto](#travis-ci-enterprise-y-cierre-del-proyecto)

# 1. Getting Started

## Todo lo que aprenderás sobre TravisCI

[Revisar sintaxis](https://config.travis-ci.com/explore)

## Continuous Integration (CI) y Continuous Delivery (CD)

![img](https://miro.medium.com/max/1400/0*TH1nBsXNDB5Njynk.PNG)

• **CI** equivale a integración continua o continuous integration, objetivo principal preparar y servir cuanto antes una release del Software de forma rápida, sencilla y ágil.

• **CD** por su parte, puede equivaler a entrega continua (continuous delivery) o despliegue continuo (continuous deployment), es el siguiente paso de la integración continua. De hecho, en muchos círculos se la conoce como una extensión de la integración continua. Su objetivo no es otro que el de proporcionar una manera ágil y fiable de poder entregar o desplegar los nuevos cambios a los clientes.

> La diferencia entre **Continuous Delivery** y **Continuous Deployment** es que en `Continuous Delivery` el desploege a ambiente de producion es **Manual** y en `Continuous Deployment` es de forma automatica.

**Términos importantes** que vamos a utilizar durante el desarrollo de Software:
**CI:** Continuous Integration.
**CD:** Continuous Delivery.
Estos 2 términos nos ayudarán a llevar de manera **más ágil nuestro código a producción.** Así como también, de hacer pruebas en ciertos bloques del código para garantizar calidad de nuestro Software.
**Fases del CI/CD:**
**[Plan]:** Requerimientos a cumplir, en este curso trabajaremos en un proyecto ya definido.
**[Code]:** Fase de desarrollo (features).
**[Build]:** Agrupación de features.
**[Test]:** Comprobación de calidad para las features.
**[Release]:** Paquete de código listo para mandar a producción.
**[Deploy]:** Software operando en un ambiente de producción.

## Crear cuenta en Travis-ci.org

Las mejores erramientas de CI/CD las siguientes:

- GitLab CI: porque puedes manejar todo todo el CI/CD de un proyecto solo usando GitLab.
- GitHub Actions: lo mismo que `GitLab CI` pero para GitHub, para mi es mi favorita, porque puedes hacer tus propias `Actios` en cualquier lenguaje de programacion, practicamente las opciones son ilimitadas.
- Jenkins: porque es la mas usada, aun que la verdad casi no me gusta.
- Travis CI: porque es super moderna y tiene integracion muy facil para desplegar en casi cualquier Cloud publica y privada.

[travis-ci.com](https://travis-ci.com/) 

[Continuous Integration como Travis](https://digital.ai/periodic-table-of-devops-tools)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.travis-ci.org/images/favicon-076a22660830dc325cc8ed70e7146a59.png)Travis CI - Test and Deploy Your Code with Confidence](https://travis-ci.org/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Travis CI User Documentation](https://docs.travis-ci.com/)

# 2. Configuración

## Archivo de configuración travis.yml

**Codigos de la clase**

```sh
//inicializar el proyecto
mkdir travis 
cd travis
git init
npm init -y
// (opcional) touch .travis.yml
code .
```

**archivo .travis.yml**

```yaml
# trabajamos con javascript
language: node_js
# os: osx
# hay mas configuraciones
git:
  # 3 niveles de git
  depth: 3

branches:
  # branches que no queremos implementar
  except:
  - legacy
  - experimental

branches:
  # solo las ramas que queremos
  only:
  - master
  - stable

before_install: #antes de hacerlo
  - python

install:
  - yarn install
# - apt install curl

script:
  - yarn deploy
  - yarn test

before_script:
  - clean

after_script:
  - yarn clean

cache:
  directories:
    - node_modules # mantener en cache node_modules
```

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)vscode-icons - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)

## Archivo de configuración travis.yml jobs y deploy

Archivo .travis.yml

```yml
// ...
jobs:
  include:
    - stage: test
      script: yarn test
      script: yarn eslint
    - stage: deploy
      script: yarn deploy

# deploy github page y heroku

deploy:
  provider: heroku
  on
    repo: danibarria/platzistore
```

- La ejecucion de tu `.travis.yml` file es un `build`, cuando travis hace su trabajo puedes decir: “esta corriendo el build”
- Un `build` tiene uno o muchos `stages` , depende de tu proceso y lo que ayas configurado en tu archivo.
- Todo `build` por defecto tiene un `stage` llamado **test**
- Si defines mas de un `stage`, esos se ejecutan **Secuencialmente** en el orden que definiste
- un `stage` tiene uno o muchos `jobs`, que corren en **paralelo**
- Un `stage` solo corre cuando todos los jos del anterior stage han pasado exitosamente
- si un job de un stage falla, los demas jobs de ese stage terminaran normalmente, pero el siguiente stage ya no correra.

> Los **flujos de trabajo** nos van a ayudar definir y a generar mediante Scripts, los requisitos para cada etapa del CI/CD.

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Travis CI User Documentation](https://docs.travis-ci.com/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - gndx/platzi-store: Curso de Pruebas unitarias con Jest](https://github.com/gndx/platzi-store)

# 3. Deploy Github Pages

## Configuración de travis.yml

Revisando la documentación de Travis, me di cuenta que hay un error en la escritura de las configuraciones de la sección deploy debido a que se está utilizando el caracter “-” en vez de “_”, siendo la forma correcta la siguiente:

```yml
lenguage: node_js
cache:
  directories:
    - node_modules
    - ~/.npm
node_js:
  - '12'
git:
  depth: 3
script:
  - yarn build
deploy:
  provider: pages
  skip_cleanup: true
  keep_history: true
  github_token: $GITHUB_TOKEN  # Set in the settings page of your repository, as a secure variable
  local_dir: dist/
  target_branch: gh-pages
  commit_message: "Deploy de proyecto"
  on:
    branch: master
```

Al final Travis pasa por alto ese detalle al momento de hacer el deploy ya que hace las validacion y al parecer corrige automáticamente, sin embargo ahi les dejo el dato.

- [GitHub Pages Deployment](https://docs.travis-ci.com/user/deployment/pages/)

**Configuraciones para el Deploy:**

Especificar el proveedor como páginas le permite a Travis CI saber su implementación en las páginas de Github.

- **skip-cleanup** se asegurará de que después de crear la carpeta que está intentando implementar, no se eliminará antes de que se implemente.

- **github-token** garantiza que la tarea de implementación tenga el privilegio suficiente para enviarla a las páginas de Github.

- **Keep-history** asegurará que no se realice un empuje forzado.

- **local-dir** es el directorio que está empujando a las páginas de Github, en nuestro caso es el sitio estático que se creó a partir de nuestro comando de script npm run styleguide:buildanterior

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - gndx/platzi-store: Curso de Pruebas unitarias con Jest](https://github.com/gndx/platzi-store)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Travis CI User Documentation](https://docs.travis-ci.com/)

## Test & Deploy de Platzi Store

Crear tocken en github

[![img](https://www.google.com/s2/favicons?domain=https://help.github.com/es/github/getting-started-with-github/set-up-git/assets/images/site/favicon.ico)Configurar Git - Ayuda de GitHub](https://help.github.com/es/github/getting-started-with-github/set-up-git)

# 4. Integración y Notificaciones

## Integración con notificaciones de correo electrónico

**codigo de la clase**

```yml
notifications:
  email:
    recipients:
    - oscar@platzi.com
    - oscar@arepa.dev
    on_success: always
    on_failure: always
# manda emails a esos mails cuando es exitoso y cuando falla
```

Para las notificaciones, podemos usar 3 diferentes valores:
**always:** always send a notification.
**never:** never send a notification.
**change:** send a notification when the build status changes.
[slack Docunentación](https://docs.travis-ci.com/user/notifications/)

**code class**

```yml
script:
  - yarn build
notifications:
  email:
    recipients:
      - conedie@gmail.com
      - diego.beltran.g@misena.edu.co
    on_success: always
    on_failure: always
deploy:
  provider: pages 
  skip-clean: true
  keep-history: true
  github-token: $GITHUB_TOKEN
  local-dir: dist/
  target-branch: gh-pages
  commit_message: "Deploy del proyecto"
  on:
    branch: master
```

## Integración con notificaciones de Slack

`.travis.yml` file

configuracion simple:

```yml
notifications:
  slack: '<account>:<token>'
```

especificando canal:

```yml
notifications:
  slack: '<account>:<token>#development'
```

multiples canales

```yml
notifications:
  slack:
    rooms:
      - <account>:<token>#development
      - <account>:<token>#general
```

**codigo de la clase**

```
notifications:
  slack: workspace:token
  # ...
```

# 5. Deploy Heroku

## Configuración de Integración

> La opción “wait for CI to pass before deploy” comprueba que las pruebas pasen correctamente antes del despliegue.

[Repo platzi-store-backend](https://github.com/gndx/platzi-store-backend)

[netlify & travis-ci](https://platzi.com/tutoriales/1843-travis/5302-deployments-unicos-por-git-branch-con-netlify-y-travis/)

## Configuración de travis.yml con Heroku

codigo de la clase

```yml
language: node_js

cache:
  directories:
    - node_modules
    - ~/.npm

node_js:
  - '12'

git:
  depth: 3

script:
  - yarn test

notifications:
  slack: workspace:token
  email:
    recipients:
      - oscar@platzi.com
      - oscar@arepa.dev
    on_success: always
    on_failure: always

deploy:
  provide: heroku
  skip-cleanup: true
  keep-history: true
  api_key: apiKey # desde heroku
  app: platzi-store-backend
  on:
    repo: danibarria/platzi-store-backend
```

## Deploy de Platzi Store Backend

Conectar Travis-CI a Github. Excelente manera de trabajar.

# 6. Seguridad

## Instalar Travis CLI (Command Line Client)

Para poder utilizar Travis CI CLI es necesario contar con Ruby en tu sistema operativo. Te dejo una serie de recursos para su instalación en los diferentes sistemas operativos:

1. [Instalar Ruby en Wiwndows](https://www.ruby-lang.org/es/documentation/installation/#rubyinstaller)
2. [Instalar Ruby en Ubuntu](https://www.ruby-lang.org/es/documentation/installation/#apt)
3. [Instalar Ruby en OS X](https://www.ruby-lang.org/es/documentation/installation/#homebrew)
4. [Otros sistemas operativos](https://www.ruby-lang.org/es/documentation/installation/)

Ahora que ya tienes instalado Ruby en tu sistema operativo vamos a proceder a instalar el cliente de línea de comandos de Travis, con el cual podemos realizar tareas específicas que nos ayuden a ser más efectivos con el uso de esta herramienta.

Para garantizar que tenemos instalado Ruby, debemos ejecutar el comando y comprobar que tenemos una versión superior a 1.9.3

```bash
ruby -v
```

Ahora lo que debemos instalar es el cliente que está disponible como una gema de Ruby y debemos de ejecutar el siguiente comando:

```bash
gem install travis
```

Una vez que la instalación se ha realizado debemos comprobar que tenemos Travis instalado con el siguiente comando:

```bash
travis version
```

Si quieres aprender más acerca del "Command Line Client" que implementa Travis CI te invito a revisar el repositorio del proyecto. [The Travis Client](https://github.com/travis-ci/travis.rb)

> Si se intenta hacer esto en produccion hay que tener cuidado que git guarda todo el historial de commits, por lo que lo mejor es eliminar (reset) los commits para que las claves no se puedan recuperar

`Travis CLI` en [**Docker**](https://hub.docker.com/r/skandyla/travis-cli/)

## Buenas practicas de seguridad

No podia encriptar las keys ya que Travis me pedia loguearme. Despues de un buen rato tratando de hacerlo pude solucionarlo de la siguiente manera 😄

### Loguearse en Travis

1. Deben crear un nuevo **github token** para iniciar sesión. Para ello ingresen a su cuenta en GitHub/settings/Developer settings/Personal access tokens/.
2. Una vez allí, generen un nuevo token y copienlo.
3. En su consola, usen el siguiente comando:

```
travis login --pro --github-token <--COPIA-TU-TOKEN-AQUÍ-->
```

1. Luego de estos pasos, deberia aparecerles que se loguearon correctamente.

### Encriptar keys

1. Para encriptar algo, deben agregarle el siguiente flag al comando visto en clase.

```
travis encrypt --com <--COPIA-TU-KEY-AQUÍ-->
```

El flag **–com** se agrega para indicarle a Travis que su repositorio y donde realizan el CI/CD se encuentra en el nuevo dominio de la plataforma ([https://www.travis-ci.com](https://www.travis-ci.com/)) y no en el anterior ([https://www.travis-ci.org](https://www.travis-ci.org/)). Por defecto, tratara de buscar los repositorios en el sitio antiguo por lo que no los encontrará y lanzará un error.

 variables de entrono de Travis, simplemente el la sección de api_key agregamos un valor con $nombre_de_la_varible

El código quedaría así

```yml
deploy:
  provider: heroku
  api_key: $APIKEYHEROKU
  app: nombre_de_la_app
```

y ponemos el nombre en la sección de variables de entorno en Travis
![Screenshot_20210414_185751.png](https://static.platzi.com/media/user_upload/Screenshot_20210414_185751-51fe59bb-3d23-4a4a-a7a1-4f395effd379.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Encryption keys - Travis CI](https://docs.travis-ci.com/user/encryption-keys/)

# 7. Travis CI Enterprise

## Travis CI Enterprise y cierre del proyecto

Desde el **2018** **Travis-CI** busca unificar todo bajo su domino **.com**, por lo que los proyectos open source y privados se deben manejar ya en **[Travis-ci.com](https://www.travis-ci.com/)** y ya no el **[travis-ci.org](https://www.travis-ci.org/)**, en este ultimo se da mantenimiento y se esta migrando al **.com**

*Over the next several months, we’ll be migrating all [travis-ci.org](https://travis-ci.org/) repositories and customers to [travis-ci.com](https://travis-ci.com/). Though this will not happen right away, you can go ahead and read more about what to expect in the [docs](https://docs.travis-ci.com/user/migrate/open-source-on-travis-ci-com/).*

[Anuncio Oficial](https://blog.travis-ci.com/2018-05-02-open-source-projects-on-travis-ci-com-with-github-apps)